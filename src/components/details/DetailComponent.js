import React from 'react'
import { Col, Row } from 'react-bootstrap'
import CardDetailComponent from '../cards/CardDetailComponent'
import TabNavigator from '../nav/TabNavigator'

const DetailComponent = () => {
  return (
    <Col>
      <TabNavigator Title="Detail music" />
      <Row xs={1} md={3} lg={4} className="p-4 m-0 d-flex  justify-content-around ">
        <CardDetailComponent />
      </Row>
    </Col>
  )
}
export default DetailComponent
