import React, { useEffect, useState } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { GetListPublicSongs } from '../../redux/SongsDuck'
import CardsComponent from '../cards/CardsComponent'
import TabNavigator from '../nav/TabNavigator'
import './home.css'

const Home = () => {
  const [currentPage, setCurrentPage] = useState(1)
  const dispatch = useDispatch()
  const prevPage = page => {
    if (currentPage + page > 0) setCurrentPage(currentPage + page)
  }

  useEffect(() => {
    dispatch(GetListPublicSongs(currentPage))
  }, [dispatch, currentPage])

  return (
    <Row>
      <TabNavigator Title="Listen to music" searchActive={true} />
      <CardsComponent />
      <Row className="align-items-center">
        <Col>
          <Button
            variant="danger"
            onClick={() => prevPage(-1)}
            className="m-2 rounded-pill text-dark Prev-Next"
          >
            Prev
          </Button>
          <Button
            variant="danger"
            onClick={() => prevPage(1)}
            className="m-2 rounded-pill text-dark Prev-Next"
          >
            Next
          </Button>
        </Col>
      </Row>
    </Row>
  )
}

export default Home
