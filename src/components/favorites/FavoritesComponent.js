import React, { Fragment } from 'react'
import CardFavoriteComponent from '../cards/CardFavoriteComponent'
import TabNavigator from '../nav/TabNavigator'

const FavoritesComponent = () => {
  return (
    <Fragment>
      <TabNavigator Title="Favorite music" />
      <CardFavoriteComponent />
    </Fragment>
  )
}
export default FavoritesComponent
