import React from 'react'
import { Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Home from '../../assets/home.jpg'
import { ROUTES } from '../../constants/routes'
import '../home/home.css'
import SearchComponent from '../search/SearchComponent'

const TabNavigator = ({ Title, searchActive = false }) => {
  return (
    <header>
      <div
        className="p-5 text-center bg-image"
        style={{ backgroundImage: `url(${Home})`, height: 200 }}
      >
        <div className="d-flex justify-content-center align-items-center h-100">
          <div className="text-white-50">
            <h1 className="mb-3">{Title}</h1>
          </div>
        </div>
      </div>
      <Nav
        className="bg-danger border-0 justify-content-end text-dark"
        defaultActiveKey={ROUTES.home}
      >
        <Link to={ROUTES.home} className=" home btn btn-outline-dark border-0 fs-5 m-2">
          Home
        </Link>
        <Link to={ROUTES.favorites} className=" home btn btn-outline-dark border-0  fs-5 m-2">
          Favorites
        </Link>
        <Link to={ROUTES.login} className=" home btn btn-outline-dark border-0 fs-5 m-2">
          LogIn
        </Link>
        {searchActive ? <SearchComponent /> : null}
      </Nav>
    </header>
  )
}

export default TabNavigator
