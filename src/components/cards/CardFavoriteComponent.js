import React, { Fragment, useEffect } from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { GetListMyFavoriteSongs } from '../../redux/SongsDuck'
import '../home/home.css'

const CardFavoriteComponent = () => {
  const dispatch = useDispatch()
  const favoriteList = useSelector(store => store.song.favoriteSongs)

  useEffect(() => {
    dispatch(GetListMyFavoriteSongs(1))
  }, [dispatch])

  return (
    <Fragment>
      <Col>
        <Row xs={1} md={3} lg={4} className="p-2 m-0 d-flex  justify-content-around ">
          {favoriteList.map(({ name, imageUrl, _id }) => (
            <Card className="card-favorite bg-dark   p-0 m-2 border border-warning" key={_id}>
              <Card.Img
                src={require(`../../assets/${imageUrl}`).default}
                alt="Card image"
                className="opacity-25"
              />
              <Card.ImgOverlay>
                <Card.Title className="fs-2  text-white-50 mt-5 fst-italic">{name}</Card.Title>
              </Card.ImgOverlay>
            </Card>
          ))}
        </Row>
      </Col>
    </Fragment>
  )
}

export default CardFavoriteComponent
