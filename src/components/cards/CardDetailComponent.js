import React from 'react'
import { Card } from 'react-bootstrap'
import { useSelector } from 'react-redux'

const CardDetailComponent = () => {
  const detail = useSelector(store => store.song.detailSong)
  return (
    <Card className="card-detail border border-warning p-0 m-0">
      <Card.Body className="text-black-50 ">
        <Card.Title className="fw-normal fs-4 text-capitalize">{detail.name}</Card.Title>
        <Card.Text className="fst-italic fs-5">{detail.description}</Card.Text>
        <Card.Text className="fst-italic">Released: {detail.released}</Card.Text>
      </Card.Body>
      <Card.Img
        variant="bottom"
        width="100%"
        src={detail._id ? require(`../../assets/${detail.imageUrl}`).default : null}
      />
    </Card>
  )
}
export default CardDetailComponent
