import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Card, Col, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { ROUTES } from '../../constants/routes'
import { ModifyFavorite, SetDetailSong } from '../../redux/SongsDuck'
import '../home/home.css'

const CardsComponent = () => {
  const dispatch = useDispatch()
  const songList = useSelector(store => store.song.publicSongs)

  return (
    <Col>
      <Row xs={1} md={3} lg={4} xl={4} xxl={4} className="p-4 m-0 d-flex  justify-content-around ">
        {songList?.map(({ name, _id, imageUrl, isMyFavorite, description, released }) => (
          <Card
            className="card-home rounded-3 mt-2 p-0 border border-secondary"
            style={{ width: '18rem' }}
            key={_id}
          >
            <Card.Img variant="top" src={require(`../../assets/${imageUrl}`).default} />
            <Card.Body>
              <Col>
                <Card.Title className="text-secondary">{name}</Card.Title>
              </Col>
              <Col>
                <Link
                  to={ROUTES.detail}
                  onClick={() =>
                    dispatch(
                      SetDetailSong({ name, _id, imageUrl, isMyFavorite, description, released })
                    )
                  }
                  className="btn btn-outline-danger"
                >
                  See more
                </Link>
              </Col>
              <Col>
                <FontAwesomeIcon
                  icon={faStar}
                  onClick={() =>
                    dispatch(ModifyFavorite({ id: _id, action: isMyFavorite ? 'Remove' : 'Add' }))
                  }
                  className={isMyFavorite ? 'likeOn' : 'likeOff'}
                />
              </Col>
            </Card.Body>
          </Card>
        ))}
      </Row>
    </Col>
  )
}

export default CardsComponent
