import { Col, Form, FormControl } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { SearchSong } from '../../redux/SongsDuck'

const SearchComponent = () => {
  const dispatch = useDispatch()
  return (
    <Col xs="4" md="4" lg="4" className="mt-2 mb-2 me-2">
      <Form className="d-flex   ms-3">
        <FormControl
          type="search"
          placeholder="Search"
          className="mr-0"
          aria-label="Search"
          onChange={e => dispatch(SearchSong(e.target.value))}
        />
      </Form>
    </Col>
  )
}

export default SearchComponent
