import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Image, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import Movil from '../../assets/movil.jpg'
import Wed from '../../assets/onda.jpg'
import { ROUTES } from '../../constants/routes'
import { ValidateUser, ValidToken } from '../../redux/userDuck'
import './login.css'

const LogIn = () => {
  const [formLogin, setFormLogin] = useState({})
  const [formValid, setFormValid] = useState(false)
  const validLogin = useSelector(store => store.user.validLogin)

  const dispatch = useDispatch()

  const validateLogin = e => {
    e.preventDefault()
    const form = e.currentTarget
    if (form.checkValidity() === true) {
      dispatch(ValidateUser(formLogin))
    } else {
      setFormValid(true)
    }
  }

  useEffect(() => {
    dispatch(ValidToken())
  }, [dispatch])

  return validLogin ? (
    <Redirect from="/" to={ROUTES.home} />
  ) : (
    <Row className="p-3 d-flex align-items-center  vh-100  ">
      <Col xs={12} lg={7}>
        <picture>
          <source srcSet={Movil} media="(max-width:767px)" />
          <source srcSet={Wed} media="(min-width:768px)" />
          <Image className="img-fluid" width="100%" src={Wed} />
        </picture>
      </Col>
      <Col xs={12} lg={4}>
        <h2 className="text-white-50 fw-normal fs-1">Sing in to continue</h2>
        <Form noValidate validated={formValid} onSubmit={e => validateLogin(e)}>
          <Form.Floating className="mb-3">
            <Form.Control
              id="floatingInputCustom"
              type="email"
              name="email"
              onChange={e => setFormLogin({ ...formLogin, email: e.target.value.trim() })}
              placeholder="name@example.com"
            />
            <label htmlFor="floatingInputCustom">Email address</label>
          </Form.Floating>
          <Form.Floating>
            <Form.Control
              id="floatingPasswordCustom"
              type="password"
              name="password"
              onChange={e => {
                setFormLogin({ ...formLogin, password: e.target.value.trim() })
              }}
              placeholder="Password"
            />
            <label htmlFor="floatingPasswordCustom">Password</label>
          </Form.Floating>
          <Button variant="danger" type="submit" className="btn-login rounded-pill mt-5 w-100">
            SING IN
          </Button>
        </Form>
      </Col>
    </Row>
  )
}

export default LogIn
