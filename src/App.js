import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import './App.css'
import DetailComponent from './components/details/DetailComponent'
import FavoritesComponent from './components/favorites/FavoritesComponent'
import Home from './components/home/home'
import LogIn from './components/login/login'
import { ROUTES } from './constants/routes'
import generateStore from './redux/store'

const store = generateStore()
const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div className="App vh-100">
        <Redirect from="/" to={ROUTES.login} />
        <Switch>
          <Route exact path={ROUTES.login} component={LogIn} />
          <Route exact path={ROUTES.home} component={Home} />
          <Route exact path={ROUTES.detail} component={DetailComponent} />
          <Route exact path={ROUTES.favorites} component={FavoritesComponent} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
)

export default App
