import axios from 'axios'
export const axiosInstance = axios.create()

axiosInstance.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.request.use(function(config) {
  const token = `Bearer ${localStorage.getItem('token')}`
  config.headers.Authorization = token
  config.url = `http://localhost${config.url}`
  return config
})
