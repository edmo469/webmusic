export const ROUTES = {
  menu: '',
  login: '/page/login',
  home: '/page/home',
  favorites: '/page/favorites/',
  detail: '/page/details'
}
