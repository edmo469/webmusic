export const SONG_ENDPOINTS = {
  GET_ALL_FAVORITE_SONGS: '/api/getFavoriteSongs',
  GET_PUBLIC_SONGS: '/api/getAllSongs',
  EDIT_FAVORITE_SONGS: '/api/editFavoriteSong'
}

export const USER_ENDPOINTS = {
  VALIDATE_USER: '/auth/validateUser',
  VALIDATE_TOKEN: '/auth/validToken'
}
