import { SONG_ENDPOINTS } from '../constants/Endpoints'
import { axiosInstance } from '../utils/axiosConfig'
//constants
const initialData = {
  basePublicSongs: [],
  publicSongs: [],
  favoriteSongs: [],
  detailSong: {}
}

const SET_PUBLIC_SONGS = 'SET_PUBLIC_SONGS'
const SET_MY_FAVORITES_SONGS = 'SET_MY_FAVORITES_SONGS'
const SET_DETAIL_SONG = 'SET_DETAIL_SONG'
const SET_FILTER_SONGS = 'SET_FILTER_SONGS'

//reducer
export default function menuReducer(state = initialData, action) {
  switch (action.type) {
    case SET_PUBLIC_SONGS:
      return {
        ...state,
        publicSongs: action.payload,
        basePublicSongs: action.payload
      }
    case SET_MY_FAVORITES_SONGS:
      return {
        ...state,
        favoriteSongs: action.payload
      }
    case SET_DETAIL_SONG:
      return {
        ...state,
        detailSong: action.payload
      }
    case SET_FILTER_SONGS:
      return {
        ...state,
        publicSongs: action.payload
      }

    default:
      return state
  }
}

export const GetListPublicSongs = (page, limit) => async (dispatch, getState) => {
  try {
    let { data } = await axiosInstance.get(`${SONG_ENDPOINTS.GET_PUBLIC_SONGS}?page=${page}`)
    dispatch({
      type: SET_PUBLIC_SONGS,
      payload: data
    })
  } catch (error) {
    console.log(error)
  }
}

export const GetListMyFavoriteSongs = () => async (dispatch, getState) => {
  try {
    let { data } = await axiosInstance.get(`${SONG_ENDPOINTS.GET_ALL_FAVORITE_SONGS}`)
    dispatch({
      type: SET_MY_FAVORITES_SONGS,
      payload: data
    })
  } catch (error) {
    console.log(error)
  }
}

export const SetDetailSong = (song = {}) => async (dispatch, getState) => {
  try {
    dispatch({
      type: SET_DETAIL_SONG,
      payload: song
    })
  } catch (error) {
    console.log(error)
  }
}

export const ModifyFavorite = favoriteAction => async (dispatch, getState) => {
  try {
    const { status, data } = await axiosInstance.patch(
      `${SONG_ENDPOINTS.EDIT_FAVORITE_SONGS}`,
      favoriteAction
    )
    if (status === 200) {
      let currentSongs = getState().song.publicSongs.map(song => {
        if (song._id === data._id) {
          song.isMyFavorite = !song.isMyFavorite
          return song
        }
        return song
      })
      dispatch({
        type: SET_PUBLIC_SONGS,
        payload: currentSongs
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export const SearchSong = songName => async (dispatch, getState) => {
  try {
    let allPublicSongs = getState().song.basePublicSongs
    let filterSongs = allPublicSongs.filter(song =>
      song.name.toLowerCase().includes(songName.toLowerCase())
    )
    dispatch({
      type: SET_FILTER_SONGS,
      payload: songName ? filterSongs : allPublicSongs
    })
  } catch (error) {
    console.log(error)
  }
}
