import { USER_ENDPOINTS } from '../constants/Endpoints'
import { axiosInstance } from '../utils/axiosConfig'
//constants
const initialData = {
  validLogin: false
}

const SET_LOGGED_USER = 'SET_LOGGED_USER'

//reducer
export default function menuReducer(state = initialData, action) {
  switch (action.type) {
    case SET_LOGGED_USER:
      return {
        ...state,
        validLogin: action.payload
      }

    default:
      return state
  }
}

export const ValidateUser = formLogin => async (dispatch, getState) => {
  try {
    let { data, status } = await axiosInstance.post(`${USER_ENDPOINTS.VALIDATE_USER}`, formLogin)
    localStorage.setItem('token', data)
    dispatch({
      type: SET_LOGGED_USER,
      payload: status === 200
    })
  } catch (error) {
    console.log(error)
  }
}

export const ValidToken = () => async (dispatch, getState) => {
  try {
    let token = localStorage.getItem('token')
    if (token) {
      let { status } = await axiosInstance.get(`${USER_ENDPOINTS.VALIDATE_TOKEN}`)
      dispatch({
        type: SET_LOGGED_USER,
        payload: status === 200
      })
    }
  } catch (error) {
    console.log(error)
  }
}
