# Web Music #

This project is related to a basic page of music, you can add favorite songs and filter it

![](example.gif)

* Version 1.0.0

## Before you begin ##

* Download and run the app <https://bitbucket.org/edmo469/apimusic/src/master/>
* Run command npm i to install packages
* Execute the command npm start to run the app